package SelBasics;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class WebOps2 {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver","C:\\chromedriver_win32\\chromedriver.exe"); //Here we enter the path location of the driver
        WebDriver driver=new ChromeDriver();
        driver.get("http://magnus.jalaacademy.com/");
        driver.findElement(By.name("UserName"));
        //Creating a textbox webElement
        WebElement element =driver.findElement(By.name("UserName"));
        //Using sendKeys to write in the textbox
        element.sendKeys("training@jalaacademy.com");
        driver.findElement(By.name("Password"));
        //Creating a textbox webElement
        WebElement element2 =driver.findElement(By.name("Password"));
        //Using sendKeys to write in the textbox
        element2.sendKeys("jobprogram");
        driver.findElement(By.xpath("//button[@id='btnLogin']")).click();


      /* driver.findElement(By.xpath("//*[@id=\"MenusDashboard\"]/li[2]")).click();
         //first create a web element list which contains all elements inside a list:

        List<WebElement> elems = driver.findElements(By.cssSelector("#MenusDashboard"));

        //Now you can select individual elements from a list using:

        elems.get(0).click();//for the 1st element
        elems.get(1).click();//for the 2nd element
       // elems.get(2).click();//for the 3rd element
        */
        //                    LINK

        // Clicking a link by using linktext and partiallinktext
       /* driver.findElement(By.linkText("http://jalatechnologies.com/contact-us.html")).click();
        driver.findElement(By.partialLinkText("http://jalatechnolog"));
        //to print all links on a webpage
        List<WebElement> allLinks = driver.findElements(By.tagName("a"));
        //Traversing through the list and printing its text along with link address
        for(WebElement link:allLinks){
            System.out.println(link.getText() + " - " + link.getAttribute("href"));
        } */
        //                   XPATH

        // Reading label and colour
      /* WebElement label= driver.findElement(By.xpath("//body[1]/div[2]/header[1]/a[1]/span[2]/b[1]"));
        String c;
        c = label.getAttribute("class");
        System.out.println("Value of label attribute: "+c); */
        // Getting ID, name and classname with Xpath
        // Getting classname
        WebElement Usname= driver.findElement(By.xpath("//input[@id='UserName']"));
        String u;
        u = Usname.getAttribute("class");
        System.out.println("Value of class attribute: "+u);
        // Getting ID name
        WebElement Idname= driver.findElement(By.xpath("//input[@id='UserName']"));
        String I;
        I = Idname.getAttribute("id");
        System.out.println("Value of ID attribute: "+I);
        // Getting name
        WebElement name= driver.findElement(By.xpath("//input[@id='UserName']"));
        String N;
        N = name.getAttribute("name");
        System.out.println("Value of name attribute: "+N);
        // Finding element using contains()
        WebElement Clname = (WebElement) driver.findElements(By.xpath("//h1[contains(text(),'Welcome to Magnus')]"));
        String W;
        W = Clname.getAttribute("name");
        System.out.println("Value of name from contains() is  "+W);
        // Finding elements using text()
        WebElement textDemo = driver.findElement(By.xpath("//*[text()='Welcome to Magnus']"));

        if(textDemo.isDisplayed())
        {
            System.out.println("Element found using text");
        }

        else
            System.out.println("Element not found");
        // Finding elements using starts-with()
        WebElement Fname = driver.findElement(By.xpath("//*[starts-with(@id,'divLoader')]"));
        String P;
        P = Fname.getAttribute("id");
        System.out.println(" "+P);

    }
}
