package SelBasics;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;
public class WebOps{
    public static void main(String[] args) {
       System.setProperty("webdriver.chrome.driver","C:\\chromedriver_win32\\chromedriver.exe"); //Here we enter the path location of the driver
       WebDriver driver=new ChromeDriver();
       // driver.manage().window().maximize();
        driver.get("http://magnus.jalaacademy.com/");
        driver.findElement(By.name("UserName"));
        //Creating a textbox webElement
        WebElement element =driver.findElement(By.name("UserName"));
        //Using sendKeys to write in the textbox
        element.sendKeys("training@jalaacademy.com");
        String val = element.getAttribute("value");
        System.out.println("Entered text is: " + val);
        // Reading Placeholder text using GETATTRIBUTE()
        WebElement TextBox= driver.findElement(By.xpath("//input[@id='Password']"));
        /* install Chropath and when you inspect an element you can use copy xpath address
        directly from the chropath plugin*/
        String l;
        l=TextBox.getAttribute("placeholder"); // Here we specify the wanted attribute value
        System.out.println("Value of placeholder attribute: "+l);
        //deleting text after entering it in a text box
        WebElement elementDel =driver.findElement(By.name("Password"));
        //Using sendKeys to write in the textbox
        elementDel.sendKeys("12345");
        elementDel.clear();
        // Checking if data value is t or f
        WebElement Var1= driver.findElement(By.xpath("//input[@id='Password']"));
        /* install Chropath and when you inspect an element you can use copy xpath address
        directly from the chropath plugin*/
        String j;
        j=Var1.getAttribute("data-val"); // Here we specify the wanted attribute value
        System.out.println("The Text box is enabled if data value is true");
        System.out.println("Value of the data val is: "+j);
        /*

                                         RADIO BUTTON CHECK BOX

         */
        // clicking the checkbox on the front page by label name
        driver.findElement(By.tagName("label")).click();
        // number of radio buttons in a group
       WebElement radioboxes = driver.findElement(By.xpath("//input[@type='checkbox']"));
       System.out.println("Total check boxes :"+radioboxes.getSize());
       // printing check box values
        WebElement values = driver.findElement(By.xpath("//input[@id='RememberMe']"));
        String k =values.getAttribute("id");
        System.out.println("Value of check box is : "+k);
        // Checking if the desired check box is selected or not
        String str = driver.findElement(By.id("RememberMe")).getAttribute("value");
        if (str.equalsIgnoreCase("true"))
        {
            System.out.println("Checkbox selected");
        }
        //  Validate Radio button using isEnabled() method checking if its enabled or not
        WebElement radioElement = driver.findElement(By.id("RememberMe"));
        boolean selectState = radioElement.isEnabled();

        //performing click operation only if element is not selected
        if(selectState == false) {
            radioElement.click();
        } else {
            System.out.println("Check box is already selected");
        }







    }
}