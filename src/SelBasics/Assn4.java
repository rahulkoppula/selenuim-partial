package SelBasics;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class Assn4 {
    public static void main(String[] args) throws NoAlertPresentException,InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver_win32\\chromedriver.exe"); //Here we enter the path location of the driver
        WebDriver driver = new ChromeDriver();
        driver.get("http://magnus.jalaacademy.com/");
        driver.findElement(By.xpath("//button[@id='btnLogin']")).click();
        /*Alert alert = driver.switchTo().alert(); // switch to alert
        String alertMessage= driver.switchTo().alert().getText(); // capture alert message

        System.out.println(alertMessage); // Print Alert Message
        alert.accept(); */

        /*
        //switch focus to alert
        Alert a = driver.switchTo().alert();
        //get alert text
        String s= driver.switchTo().alert().getText();
        System.out.println("Alert text is: " + s);
        //accepting alert
        a.accept();   */
        // Here we guide the bot to the x on the pop up message box and make it click it to as to close it
        driver.findElement(By.xpath("//body/div[@id='toast-container']/div[1]/button[1]")).click();
        // Here we make the bot to open a link in new tab
        // Keys.Chord string
        String clicklnk = Keys.chord(Keys.CONTROL, Keys.ENTER);
        // open the link in new tab, Keys.Chord string passed to sendKeys
        driver.findElement(
                By.xpath("//a[contains(text(),'Admin Login')]")).sendKeys(clicklnk);
        // this is to switch to the new opened tab
        String currentHandle = driver.getWindowHandle();
        Set<String> handles = driver.getWindowHandles();
        for (String actual : handles) {
            if (!actual.equalsIgnoreCase(currentHandle)) {
                //Switch to the opened tab
                driver.switchTo().window(actual);}}

            // TD --> HANDLES, DROPDOWN, DISMISS AND ALERTS
    }
}